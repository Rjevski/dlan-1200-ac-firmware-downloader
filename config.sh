
PIB_FILE='qca7500-pib15-devolo-mt2675.pib' # depends on hardware
NVM_FILE='MAC-7500-v2.6.0-01-NW6__-X-CS.nvm'

REPO_URL='https://github.com/devolo/dlan-openwrt.git'
DEB_URL='https://update.devolo.com/linux2/apt/pool/main/d/devolo-firmware-dlan1200-wifiac/devolo-firmware-dlan1200-wifiac_5.2.1-3_i386.deb'
