# Devolo dLAN 1200 WiFi AC firmware downloader

## Summary

This repo contains scripts to build an OpenWrt feed containing the firmware for a dLAN 1200 WiFi AC powerline adapter.

It works by downloading a `.deb` file of the proprietary firmware from Devolo (URLs in `config.sh`) as well as a copy of the `dlan-openwrt` repo. It then extracts the deb and the firmware image contained inside, and moves the files in the proper directory.

The feed can then be used as a local directory-based feed for your OpenWrt build environment.

## Requirements

* dpkg
* git
* binwalk
* squashfs

## Configuration

There are several configuration variables in `config.sh`.

You will need to set the `PIB_FILE` variable to match your hardware. Behind your powerline adapter you'll find a string like `MT2675`. Adjust the number in the variable to match the markings on your hardware.

If you are getting a 404 when attempting to download the file it may mean the Deb file no longer exists and may have been replaced by a newer version. In this case set `DEB_URL` to the latest file at https://update.devolo.com/linux2/apt/pool/main/d/devolo-firmware-dlan1200-wifiac/.


## Usage

Install the dependencies (depending on your distribution the packages might be named differently; `squashfs` is `squashfs-tools` on Debian and Ubuntu for example) then run `./get_firmware.sh`. At the end it will print a line you need to add to your OpenWrt `feeds.conf`; then run `./scripts/feeds update dlan` and `./scripts/feeds install dlan` then select the appropriate firmware package in `make menuselect` and finally build OpenWrt as normal.