#!/bin/bash
set -e

. config.sh

DEB_FILENAME='firmware.deb'
DEB_TARGET_DIR='deb'
FW_TARGET_DIR='fw'
REPO_TARGET_PATH='feeds'

curl -o $DEB_FILENAME $DEB_URL
dpkg -x $DEB_FILENAME $DEB_TARGET_DIR
binwalk $DEB_TARGET_DIR/firmware/devolo-firmware-dlan1200-wifiac/delos_dlan-1200-ac_5.2.1_2019-02-19.bin.dvl -e -C $FW_TARGET_DIR

git clone $REPO_URL $REPO_TARGET_PATH
cp $FW_TARGET_DIR/_delos_dlan-1200-ac_5.2.1_2019-02-19.bin.dvl.extracted/squashfs-root/lib/firmware/plc/dlan-pro-1200-ac/{$PIB_FILE,$NVM_FILE} $REPO_TARGET_PATH/dlan-fw/qca/dlan-pro-1200-ac

FEEDS_PATH=$(realpath $REPO_TARGET_PATH)

echo "Add 'src-cpy dlan $FEEDS_PATH' to your feeds.conf in your OpenWrt build environment."

rm -rf "$DEB_FILENAME" "$DEB_TARGET_DIR" "$FW_TARGET_DIR"
